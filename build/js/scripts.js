ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map('map', {
        center: [59.90728406421673, 30.3178355],
        zoom: 15,
    });
    myMap.geoObjects.add(
        new ymaps.Placemark(
            [59.90728406421673, 30.3178355],
            {
                hintContent: 'Московский проспект, 65',
                balloonContent: 'Московский проспект, 65',
            },
            {
                preset: 'islands#icon',
                iconColor: 'red',
            },
        ),
    );

    myMap.behaviors.disable('scrollZoom');
}

$(document).ready(function() {
  $('input[type="tel"]').mask('+7(999) 999 99 99');

$('.popup').magnificPopup({
    type: 'inline',
    removalDelay: 350,
    midClick: !0,
    tLoading: '',
    mainClass: 'mfp-fade'
});

$('.apartment--thumbnail').magnificPopup({
    delegate: 'a',
    preloader: false,
    type: 'image',
    removalDelay: 350,
    midClick: !0,
    tLoading: '',
    mainClass: 'mfp-fade'
  });
})
